#include "roadnet.h"
#include "graphicsitem.h"

#include <QDebug>

RoadNet::RoadNet()
{

}

RoadNet::~RoadNet()
{
    qDeleteAll(_roadLinks);
    qDeleteAll(_roadLinksGraphics);
}

QList<QGraphicsItem *> RoadNet::drawNetwork()
{
    _roadLinksGraphics.clear();
    for(auto const &roadLink: _roadLinks) {
        RoadLinkItem* line = new RoadLinkItem(roadLink->getLine());
        _roadLinksGraphics.append(line);
    }
    return _roadLinksGraphics;
}

void RoadNet::setVisible(bool visible)
{
    for(auto &x: _roadLinksGraphics) {
        x->setVisible(visible);
    }
}

void RoadNet::addLink(QString id, QString description, double x1, double y1, double x2, double y2)
{
    if(!_roadLinks.contains(id)) {
        RoadLink* roadLink = new RoadLink(id, description, x1, y1, x2, y2);
        _roadLinks.insert(id, roadLink);
    }
}

bool RoadNet::isEmpty()
{
    return _roadLinks.isEmpty() || _roadLinksGraphics.isEmpty();
}

