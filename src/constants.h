#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QString>
#include <QColor>

const int MAX_LOCATIONS_MAP_MATCHING = 1000;
const int WAYPOINT_SIZE = 5;
const QString PROJ_IN  = "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs"; // WGS84
//const QString PROJ_IN = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs";
const QString PROJ_OUT = "+proj=tmerc +lat_0=49 +lon_0=-2 +k=0.9996012717 +x_0=400000 +y_0=-100000 +ellps=airy +datum=OSGB36 +units=m +no_defs";
const QColor ORANGE = QColor(255, 145, 0);
const QColor GREEN  = QColor(21, 148, 25);
const QColor PURPLE = QColor(153, 14, 240);
const QColor BLUE   = QColor(0, 17, 255);
const QColor RED    = QColor(255, 14, 0);
const QColor YELLOW = QColor(237, 237, 24);


#endif // CONSTANTS_H

