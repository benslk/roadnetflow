#ifndef NETLOADER_H
#define NETLOADER_H

#include <QObject>
#include <QPointF>
#include <QGraphicsItem>

#include "loader.h"
#include "roadnet.h"

class NetLoader: public Loader
{
public:
    NetLoader(MainWindow* parent = 0, QString filename="", QString delimiter=",", bool header=false):
        Loader(parent, filename), _delimiter(delimiter), _header(header)
    {
        _roadNet = new RoadNet();
    }

    void load();
    QList<QGraphicsItem*> drawNetwork();
    void setVisible(bool visible);

private:
    bool concurrentLoad();

    QString _delimiter;
    bool _header;
    RoadNet* _roadNet;
};

#endif // NETLOADER_H
