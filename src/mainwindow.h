#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "netloader.h"
#include "shapefileloader.h"
#include "customscene.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();

private slots:
    void openNetFile();
    void openFlowFile();
    void openShapefile();

private:
    Ui::MainWindow* ui;
    CustomScene* _scene;
    NetLoader* _netLoader;
    ShapefileLoader* _shapefileLoader;
    bool _showShapefile;
    bool _showRoadNet;
};

#endif // MAINWINDOW_H
