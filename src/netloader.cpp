#include "netloader.h"

#include "graphicsitem.h"

void NetLoader::load()
{
    _loadResult = QtConcurrent::run(this, &NetLoader::concurrentLoad);
}

QList<QGraphicsItem*> NetLoader::drawNetwork()
{
    return _roadNet->drawNetwork();
}

void NetLoader::setVisible(bool visible)
{
    _roadNet->setVisible(visible);
}

bool NetLoader::concurrentLoad()
{
    if(!_roadNet->isEmpty()) {
        delete _roadNet;
        _roadNet = new RoadNet();
    }

    QFile* file = new QFile(_filename);

    if(!file->open(QFile::ReadOnly | QFile::Text))
    {
        return false;
    }
    int lineNbr = 0;
    if(_header)
    {
        // skip the header
        file->readLine();
    }

    // read the file
    while(!file->atEnd())
    {
        QString line = QString(file->readLine()).split(QRegExp("[\r\n]"), QString::SkipEmptyParts).at(0);
        QStringList fields = line.split(_delimiter);
        QString id = fields.at(0);
        QString description = fields.at(1);
        double x1 = fields.at(4).toDouble();
        double y1 = fields.at(5).toDouble();
        double x2 = fields.at(6).toDouble();
        double y2 = fields.at(7).toDouble();

        _roadNet->addLink(id, description, x1, y1, x2, y2);
        
        // Indicate the load progress of the file
        if(lineNbr % 1000 == 0) {
            qreal loadProgress = 1.0 - file->bytesAvailable() / (qreal)file->size();
            emit loadProgressChanged(loadProgress);
        }
        lineNbr++;
    }

    emit loadProgressChanged((qreal)1.0);
    qDebug() << "parsed" << lineNbr << "lines";

    return true;
}
